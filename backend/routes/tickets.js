const express = require('express');

const Ticket = require("../models/ticket");

const router = express.Router();

router.post("", (req, res, next) => {
  const ticket = new Ticket({
    title: req.body.title,
    status: req.body.status,
    content: req.body.content
  });
  ticket.save().then(createdTicket => {
    res.status(201).json({
      message: "Ticket added Successfully",
      ticketId: createdTicket._id
    }); //201 means everything is success and new resource was created.
  });
});

router.get("", (req, res, next) => {
  Ticket.find().then(documents => {
    res.status(200).json({
      //status 200 means success.
      message: "Posts fetched successfully!",
      tickets: documents
    });
  });
});

router.put("/:id", (req, res, next) => {
  const ticket = new Ticket({
    _id: req.body.id,
    title: req.body.title,
    status: req.body.status,
    content: req.body.content
  });
  Ticket.updateOne({ _id: req.params.id }, ticket).then(result => {
    res.status(200).json({ message: "update successful!" });
  });
});

router.get("/:id", (req, res, next) => {
  Ticket.findById(req.params.id).then(ticket => {
    if (ticket) {
      res.status(200).json(ticket);
    } else {
      res.status(404).json({ message: "Ticket not found!" });
    }
  });
});

router.delete("/:id", (req, res, next) => {
  Ticket.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({ message: "Ticket deleted!" });
  });
});

module.exports = router;
