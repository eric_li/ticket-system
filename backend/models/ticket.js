const mongoose = require('mongoose');

const ticketSchema = mongoose.Schema({
  title: { type: String, required: true },
  status: { type: String, required: true },
  content: { type: String, required: true }
});

module.exports = mongoose.model('Ticket', ticketSchema);
