const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const ticketsRoutes = require('./routes/tickets');

const app = express();

mongoose.connect("mongodb+srv://Eric:1234@cluster0.tmskl.mongodb.net/node-angular?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('connected to database');
  })
  .catch(() => {
    console.log('connection failed');
  });

// Eric 1234 This is the user in mongoDB

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use('/api/posts', ticketsRoutes);

module.exports = app;
