export interface Ticket {
  id: string;
  title: string;
  status: string;
  content: string;
}
