import { Router } from '@angular/router';
import { Ticket } from './post.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private tickets: Ticket[] = [];
  private ticketsUpdated = new Subject<Ticket[]>();

  constructor(private http: HttpClient, private router: Router) {}

  getTickets() {
    this.http
      .get<{ message: string; tickets: any }>('http://localhost:3000/api/posts')
      .pipe(
        map(ticketData => {
          return ticketData.tickets.map(ticket => {
            return {
              title: ticket.title,
              status: ticket.status,
              content: ticket.content,
              id: ticket._id
            };
          });
        })
      )
      .subscribe(transformedTickets => {
        this.tickets = transformedTickets;
        this.ticketsUpdated.next([...this.tickets]);
      });
  }

  getTicketUpdateListener() {
    return this.ticketsUpdated.asObservable();
  }

  getTicket(id: string) {
    return this.http.get<{
      _id: string;
      title: string;
      status: string;
      content: string;
    }>('http://localhost:3000/api/posts/' + id);
  }

  addTicket(title: string, status: string, content: string) {
    const ticket: Ticket = {
      id: '',
      title: title,
      status: status,
      content: content
    };
    this.http
      .post<{ message: string; ticketId: string }>(
        'http://localhost:3000/api/posts',
        ticket
      )
      .subscribe(responseData => {
        const id = responseData.ticketId;
        ticket.id = id;
        this.tickets.push(ticket);
        this.ticketsUpdated.next([...this.tickets]);
        this.router.navigate(['/']);
      });
  }

  updateTicket(id: string, title: string, status: string, content: string) {
    const ticket: Ticket = {
      id: id,
      title: title,
      status: status,
      content: content
    };
    this.http.put('http://localhost:3000/api/posts/' + id, ticket)
      .subscribe(response => {
        const updatedTickets = [...this.tickets];
        const oldTicketIndex = updatedTickets.findIndex(
          t => t.id === ticket.id
        );
        updatedTickets[oldTicketIndex] = ticket;
        this.tickets = updatedTickets;
        this.ticketsUpdated.next([...this.tickets]);
        this.router.navigate(["/"]);
      });
  }

  deleteTicket(ticketId: string) {
    this.http
      .delete('http://localhost:3000/api/posts/' + ticketId)
      .subscribe(() => {
        const updatedTickets = this.tickets.filter(
          ticket => ticket.id !== ticketId
        );
        this.tickets = updatedTickets;
        this.ticketsUpdated.next([...this.tickets]);
      });
  }
}
