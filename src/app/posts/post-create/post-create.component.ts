import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { PostsService } from '../posts.service';
import { Ticket } from '../post.model';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  ticketTitle = '';
  ticketStatus = '';
  ticketContent = '';
  ticket: Ticket;
  isLoading = false;
  private mode = 'create';
  private ticketId: string;

  constructor(
    public postsService: PostsService,
    public route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: paramMap) => {
      if (paramMap.has('ticketId')) {
        this.mode = 'edit';
        this.ticketId = paramMap.get('ticketId');
        this.isLoading = true;
        this.postsService.getTicket(this.ticketId)
          .subscribe(ticketData => {
            this.isLoading = false;
            this.ticket = {
              id: ticketData._id,
              title: ticketData.title,
              status: ticketData.status,
              content: ticketData.content
            };
          });
      } else {
        this.mode = 'create';
        this.ticketId = '';
      }
    });
  }

  onSaveTicket(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'create'){
      this.postsService.addTicket(form.value.title, form.value.status, form.value.content);
    } else {
      this.postsService.updateTicket(this.ticketId, form.value.title, form.value.status, form.value.content);
    }
    form.resetForm();
  }
}
