import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ticket } from '../post.model';
import { PostsService } from '../posts.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
  // tickets =[
  //   { title: 'First ticket', content: 'First ticket\'s content', file: 'First documents here' },
  //   { title: 'Second ticket', content: 'Second ticket\'s content', file: 'Second documents here' },
  //   { title: 'Third ticket', content: 'Third ticket\'s content', file: 'Third documents here' },
  // ]
  tickets: Ticket[] = [];
  isLoading = false;
  private ticketsSub: Subscription;

  constructor(public postsService: PostsService) {}

  ngOnInit() {
    this.isLoading = true;
    this.postsService.getTickets();
    this.ticketsSub = this.postsService
      .getTicketUpdateListener()
      .subscribe((tickets: Ticket[]) => {
        this.isLoading = false;
        this.tickets = tickets;
      });
  }

  onDelete(ticketId: string) {
    this.postsService.deleteTicket(ticketId);
  }

  ngOnDestroy() {
    this.ticketsSub.unsubscribe();
  }
}
